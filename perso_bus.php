<?php

//----------------------RETOUR MAISON 206 ------------------------------------------------------------------------------------
$retour_maison206='https://www.ratp.fr/horaires?network-current=&networks=busratp&line_busratp=206&name_line_busratp=Gare+de+Pontault-Combault+%2F+Noisy+le+Grand+RER&id_line_busratp=B206&line_noctilien=&name_line_noctilien=&id_line_noctilien=&directions=A&stop_point_busratp=Gare+Villiers-S-Marne+RER&type=now&departure_date=15%2F02%2F2018&departure_hour=12&departure_minute=20&op=Rechercher&form_build_id=form-sALwkaRqLoWly1bLiSR2vmaM1RA5VTFIpD-eaLenX_M&form_id=scheduledform';

function get_infos_bus_retour206($chaine){
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $chaine);
	
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	$return = curl_exec($curl);
	
	$begin=strpos($return,'<div class="network-directions"><strong class="directions">Gare de Pontault-Combault</strong></div>');
	$chaine_restante=substr($return,$begin, 3000);
	$pos_fin=stripos($chaine_restante,'<div class="ixxi-horaire-result-wrapper ixxi-horaire-download">');
	$probleme = substr($chaine_restante,15,($pos_fin));
	
	$tab_probleme=explode('<li class',$probleme);
	
	$tab_retour=array();
	$tab_retour[0]=str_replace('="body-busratp">','',$tab_probleme[2]);
	$tab_retour[1]=str_replace('="body-busratp">','',$tab_probleme[3]);
	return($tab_retour);
	
}


$tab_retour_206=get_infos_bus_retour206($retour_maison206);
echo "<font size=\"+6\"><center><b><font color=\"008000\">206 Direction MAISON</font><br>";
foreach($tab_retour_206 as $k=>$v){
	echo $v."<br>";
}
echo "</b></center></font>";

// -----------------------------------------------------------RETOUR MAISON 207 ---------------------------------------------------------------------
$retour_maison207='https://www.ratp.fr/horaires?network-current=busratp&networks=busratp&line_busratp=207&name_line_busratp=Hopital+Queue+en+Brie+%2F+Noisy+le+Grand+RER&id_line_busratp=B207&line_noctilien=&name_line_noctilien=&id_line_noctilien=&directions=A&stop_point_busratp=Gare+Villiers-S-Marne+RER&type=now&departure_date=15%2F02%2F2018&departure_hour=15&departure_minute=10&op=Rechercher&form_build_id=form-ulC38tTdZeJnCzjtrofvGivKJTXQOpGXrmsPcHMCPqY&form_id=scheduledform';

function get_infos_bus_retour207($chaine){
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $chaine);
	
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	$return = curl_exec($curl);
	
	$begin=strpos($return,'<div class="network-directions"><strong class="directions">Hopital Queue en Brie</strong></div>');
	$chaine_restante=substr($return,$begin, 3000);
	$pos_fin=stripos($chaine_restante,'<div class="ixxi-horaire-result-wrapper ixxi-horaire-download">');
	$probleme = substr($chaine_restante,15,($pos_fin));
	
	$tab_probleme=explode('<li class',$probleme);
	
	$tab_retour=array();
	$tab_retour[0]=str_replace('="body-busratp">','',$tab_probleme[2]);
	$tab_retour[1]=str_replace('="body-busratp">','',$tab_probleme[3]);
	return($tab_retour);
	
}


$tab_retour_207=get_infos_bus_retour207($retour_maison207);
echo "<font size=\"+6\"><center><hr><b><font color=\"008888\">207 Direction MAISON</font><br>";
foreach($tab_retour_207 as $k=>$v){
	echo $v."<br>";
}
echo "</b></center></font>";



//--------------------------------------------------------------------------------------ALLER VERS GARE 206 ---------------------------------------


$aller_gare206='https://www.ratp.fr/horaires?network-current=busratp&networks=busratp&line_busratp=206&name_line_busratp=Gare+de+Pontault-Combault+%2F+Noisy+le+Grand+RER&id_line_busratp=B206&line_noctilien=&name_line_noctilien=&id_line_noctilien=&directions=R&stop_point_busratp=La+Dame+Blanche&type=now&departure_date=15%2F02%2F2018&departure_hour=15&departure_minute=20&op=Rechercher&form_build_id=form-ulC38tTdZeJnCzjtrofvGivKJTXQOpGXrmsPcHMCPqY&form_id=scheduledform';

function get_infos_bus_aller206($chaine){
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $chaine);
	
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	$return = curl_exec($curl);


	$begin=strpos($return,'<div class="network-directions"><strong class="directions">Noisy le Grand RER</strong></div>');
	$chaine_restante=substr($return,$begin, 3000);
	$pos_fin=stripos($chaine_restante,'<div class="ixxi-horaire-result-wrapper ixxi-horaire-download">');
	$probleme = substr($chaine_restante,15,($pos_fin));
	
	$tab_probleme=explode('<li class',$probleme);
	
	$tab_retour=array();
	$tab_retour[0]=str_replace('="body-busratp">','',$tab_probleme[2]);
	$tab_retour[1]=str_replace('="body-busratp">','',$tab_probleme[3]);
	return($tab_retour);
	
}


	
	
//	$begin=strpos($return,'<div class="network-directions"><strong class="directions">Gare de Pontault-Combault</strong></div>');
//	$chaine_restante=substr($return,$begin, 3000);
//	$pos_fin=stripos($chaine_restante,'<div class="ixxi-horaire-result-wrapper ixxi-horaire-download">');
//	$probleme = substr($chaine_restante,15,($pos_fin));
	
//	$tab_probleme=explode('<li class',$probleme);
	
//	$tab_aller=array();
//	$tab_aller[0]=str_replace('="body-busratp">','',$tab_probleme[2]);
//	$tab_aller[1]=str_replace('="body-busratp">','',$tab_probleme[3]);
//	return($tab_aller);



$tab_aller_206=get_infos_bus_aller206($aller_gare206);

echo "><font size=\"+6\"><center><hr><hr><b><font color=\"008000\">206 Direction gare</font><br>";
foreach($tab_aller_206 as $k=>$v){
	echo $v."<br>";
}
echo "</b></center></font>";

//-----------------------------------------------------------------ALLER 207 ----------------------------------------------------------------

$aller_gare207='https://www.ratp.fr/horaires?network-current=busratp&networks=busratp&line_busratp=207&name_line_busratp=Gare+de+Pontault-Combault+%2F+Noisy+le+Grand+RER&id_line_busratp=B207&line_noctilien=&name_line_noctilien=&id_line_noctilien=&directions=R&stop_point_busratp=La+Dame+Blanche&type=now&departure_date=15%2F02%2F2018&departure_hour=15&departure_minute=20&op=Rechercher&form_build_id=form-ulC38tTdZeJnCzjtrofvGivKJTXQOpGXrmsPcHMCPqY&form_id=scheduledform';

function get_infos_bus_aller207($chaine){
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $chaine);
	
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	$return = curl_exec($curl);


	$begin=strpos($return,'<div class="network-directions"><strong class="directions">Noisy le Grand RER</strong></div>');
	$chaine_restante=substr($return,$begin, 3000);
	$pos_fin=stripos($chaine_restante,'<div class="ixxi-horaire-result-wrapper ixxi-horaire-download">');
	$probleme = substr($chaine_restante,15,($pos_fin));
	
	$tab_probleme=explode('<li class',$probleme);
	
	$tab_retour=array();
	$tab_retour[0]=str_replace('="body-busratp">','',$tab_probleme[2]);
	$tab_retour[1]=str_replace('="body-busratp">','',$tab_probleme[3]);
	return($tab_retour);
	
}


	
	
//	$begin=strpos($return,'<div class="network-directions"><strong class="directions">Gare de Pontault-Combault</strong></div>');
//	$chaine_restante=substr($return,$begin, 3000);
//	$pos_fin=stripos($chaine_restante,'<div class="ixxi-horaire-result-wrapper ixxi-horaire-download">');
//	$probleme = substr($chaine_restante,15,($pos_fin));
	
//	$tab_probleme=explode('<li class',$probleme);
	
//	$tab_aller=array();
//	$tab_aller[0]=str_replace('="body-busratp">','',$tab_probleme[2]);
//	$tab_aller[1]=str_replace('="body-busratp">','',$tab_probleme[3]);
//	return($tab_aller);



$tab_aller_207=get_infos_bus_aller207($aller_gare207);

echo "<font size=\"+6\"><center><hr><b><font color=\"008888\">207 Direction gare</font><br>";
foreach($tab_aller_207 as $k=>$v){
	echo $v."<br>";
}
echo "</b></center></font>";




?>

